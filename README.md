# Rüsselfaultier

Im Urwald von Informatien ist ein Rüsselfaultier gefunden worden. Die Forscher sind begeistert: Zum ersten Mal seit Jahrhunderten wurde ein bislang unbekanntes Großtier entdeckt. Es stellt sich die Frage, warum diese Tiere solange verborgen geblieben sind. Offenbar verfügen sie über einen besonders guten Tarnmechanismus.

Nachdem die ersten Rüsselfaultiere in einen Zoo gebracht wurden, stellten die Tierpfleger fest, dass diese – ähnlich wie Chamäleons – die Farbe ihrer Haut an beliebige Umgebungen anpassen können. Wenn ein Rüsselfaultier merkt, dass es beobachtet wird, nimmt jede seiner Hautschuppen die Umgebungsfarbe der dem Betrachter gegenüberliegenden Körperseite an. Dadurch wird es für den Beobachter quasi „durchsichtig“.

Mit modernen Digitalkameras ist es allerdings möglich, Rüsselfaultier zu erkennen: Wenn die Auflösung nur hoch genug ist, wird jede Hautschuppe durch mehrere nebeneinander liegende Pixel dargestellt. Weil alle diese Pixel dieselbe Schuppe abbilden, haben sie genau die gleiche Farbe. Dadurch kann man feststellen, welche Pixel in einem Bild möglicherweise einen Rüsselfaultier darstellen.

Es sollen nun mehrere Fotos aus dem Rüsselfaultierwald geprüft werden, ob darauf vielleicht ein Rüsselfaultier abgebildet ist. Erstelle ein Programm, das diejenigen Pixel eines Bildes weiß färbt, die zu einem Rüsselfaultier gehören könnten.